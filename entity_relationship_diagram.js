/**
 * @file
 * Entity Relationship Diagram feature.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.renderDotSVG = {
    attach: function (context, settings) {
      Viz.instance()
        .then(viz => {
          document.body.appendChild(viz.renderSVGElement(drupalSettings.entity_relationship_diagram.dataSVG));
        })
        .catch(error => {
          console.error(error);
        });
    }
  };
})(jQuery, Drupal);
